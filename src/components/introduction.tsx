import FadeInOnScroll from "../components/fadeInOnScroll";
import Image from "next/image";
import twelve from "../../public/twelve.png";

export default function Introduction() {
  return (
    <div className="h-screen w-screen bg-ghost-blue scrollbar-hide">
      <FadeInOnScroll>
        <div
          id="introduction"
          className="flex h-screen w-screen items-center justify-center text-center font-libre"
        >
          <div className="w-3/4 break-words justify-center align-middle items-center text-center whitespace-normal m-auto">
            <h1 className="xl:text-4xl lg:text-2xl py-0">INTRODUCTION</h1>
            <div className="flex flex-row items-center gap-12">
              <div>
                <p className="text-justify">
                  Ghost Stories For Large Ensemble is a project focused on
                  creating and documenting a suite of programatic original music
                  focused on exploring thematic material through the lens of
                  ghost stories.
                  <br />
                  <br />
                  What you will find below are a set of journals which document
                  the process of researching, conceptualizing and creating this
                  music. They are meant as a chronicle of the process, including
                  both the successful ideas which constitute the finished pieces
                  and failed ideas which were never realized.
                  <br />
                  <br />
                  Once recordings are created, they will be made available via{" "}
                  <a href="https://alexeiozerov.bandcamp.com">
                    <b>Bandcamp</b>
                  </a>
                  . The recordings included in the journals may be snippets from
                  rehearsals or MIDI renderings of the music.
                  <br />
                  <br />
                  My name is{" "}
                  <a href="https://alexeiozerovmusic.com">
                    <b>Alexei Ozerov</b>
                  </a>
                  , and I hope you enjoy the stories.
                </p>
              </div>
              <div className="relative overflow-hidden items-center text-center justify-center">
                <Image
                  src={twelve}
                  alt="GHOST STORIES FOR LARGE ENSEMBLE Logo"
                  quality="100"
                  placeholder="blur"
                />
              </div>
            </div>
          </div>
        </div>
      </FadeInOnScroll>
    </div>
  );
}
