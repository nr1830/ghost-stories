import FadeInOnScroll from "../components/fadeInOnScroll";

export default function Communion() {
  return (
    <div className="h-screen w-screen bg-ghost-blue">
      <FadeInOnScroll>
        <div className="flex h-screen w-screen items-center justify-center text-center font-libre">
          <div className="flex w-1/2 flex-wrap flex-row break-words justify-center align-middle text-center whitespace-normal">
            <h1 id="communion" className="xl:text-4xl lg:text-2xl py-12">
              COMMUNION
            </h1>

            <div className="grid grid-cols-2 text-justify gap-16">
              <div>
                The soloist performs an incantation to communicate with a
                many-faced being (the ensemble). As the two interact, each
                beigns to take on an aspect of the other, communicated through
                the shifting and manipulation of the 12-tone row used as the
                foundation of the suite.
              </div>
              <div>
                As I sit, thinking about how to conceptualize this, I consider a
                number of &quot;problem statements&quot; into which I can break
                the beginnings of this project down. They fall into two
                categories: (1) research and (2) pre-compositional & narrative
                planning.
              </div>
            </div>
          </div>
        </div>
      </FadeInOnScroll>
    </div>
  );
}
