import { useRef, useEffect } from "react";
import { motion, useAnimation } from "framer-motion";
import { useInView } from "react-intersection-observer";

interface FadeInOnScrollProps {
  children: React.ReactNode;
}

export default function FadeInOnScroll({
  children,
}: FadeInOnScrollProps): JSX.Element {
  const controls = useAnimation();

  {
    /*
    TODO: Create a dynamic value for rootMargin based on the viewport size
          Currently, there is an issue with rendering values on small screens
          or mobile devices.
  */
  }
  const [ref, inView] = useInView({ threshold: 0.4, rootMargin: "-20px" });
  const refCopy = useRef<HTMLDivElement | null>(null);

  useEffect(() => {
    if (inView) {
      controls.start("visible");
    } else {
      controls.start("hidden");
    }
  }, [controls, inView, refCopy]);

  return (
    <motion.div
      ref={(node) => {
        ref(node);
        refCopy.current = node;
      }}
      animate={controls}
      initial="hidden"
      exit="hidden"
      variants={{
        visible: { opacity: 1, y: 0 },
        hidden: { opacity: 0, y: 20 },
      }}
      transition={{ duration: 0.7, ease: "easeInOut" }}
    >
      {children}
    </motion.div>
  );
}
