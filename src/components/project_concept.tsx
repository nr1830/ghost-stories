import Image from "next/image";

import FadeInOnScroll from "../components/fadeInOnScroll";
import twelve from "../../public/12tonelabeled.png";

export default function WritingForeword() {
  return (
    <div className="w-screen bg-ghost-blue">
      <FadeInOnScroll>
        <div className="flex flex-col items-center justify-center text-center font-libre">
          <h1 id="journal00" className="xl:text-4xl lg:text-2xl py-12">
            JOURNAL 00: PROJECT CONCEPT
          </h1>
          <div className="flex w-3/4 m-auto flex-wrap flex-row break-words justify-center align-middle text-center whitespace-normal">
            <p className="text-justify italic pb-5 xl:pb-10">
              Ghost Stories For Large Ensemble
            </p>
            <div className="flex flex-wrap lg:columns-1 columns-2 text-justify">
              <div className="columns-2 gap-12">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam
                pulvinar enim vel tincidunt condimentum. Donec maximus arcu
                vitae lorem dignissim, et sollicitudin eros pharetra.
                Pellentesque congue venenatis turpis. Maecenas lorem massa,
                ornare in tortor at, condimentum ullamcorper metus. Duis
                ullamcorper id nulla ac congue. Cras ac sapien eu arcu bibendum
                cursus. Donec tempor placerat orci, et posuere turpis pharetra
                et. Integer aliquet leo eu nisi elementum tincidunt. Nulla
                bibendum ante luctus nisi condimentum eleifend. Donec tortor
                magna, sagittis ac felis aliquam, pulvinar tincidunt ex.
                <br />
                <br />
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam
                pulvinar enim vel tincidunt condimentum. Donec maximus arcu
                vitae lorem dignissim, et sollicitudin eros pharetra.
                Pellentesque congue venenatis turpis. Maecenas lorem massa,
                ornare in tortor at, condimentum ullamcorper metus. Duis
                ullamcorper id nulla ac congue. Cras ac sapien eu arcu bibendum
                cursus. Donec tempor placerat orci, et posuere turpis pharetra
                et. Integer aliquet leo eu nisi elementum tincidunt. Nulla
                bibendum ante luctus nisi condimentum eleifend. Donec tortor
                magna, sagittis ac felis aliquam, pulvinar tincidunt ex. <br />
                <br />
                <br />
                <h2 className="xl:text-2xl lg:text-xl pb-5">12-Tone Row</h2>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam
                pulvinar enim vel tincidunt condimentum. Donec maximus arcu
                vitae lorem dignissim, et sollicitudin eros pharetra.
                Pellentesque congue venenatis turpis. Maecenas lorem massa,
                ornare in tortor at, condimentum ullamcorper metus. Duis
                ullamcorper id nulla ac congue. Cras ac sapien eu arcu bibendum
                cursus. Donec tempor placerat orci, et posuere turpis pharetra
                et. Integer aliquet leo eu nisi elementum tincidunt. Nulla
                bibendum ante luctus nisi condimentum eleifend. Donec tortor
                magna, sagittis ac felis aliquam, pulvinar tincidunt ex.
                <br />
                <br />
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam
                pulvinar enim vel tincidunt condimentum. Donec maximus arcu
                vitae lorem dignissim, et sollicitudin eros pharetra.
                Pellentesque congue venenatis turpis. Maecenas lorem massa,
                ornare in tortor at, condimentum ullamcorper metus. Duis
                ullamcorper id nulla ac congue. Cras ac sapien eu arcu bibendum
                cursus. Donec tempor placerat orci, et posuere turpis pharetra
                et. Integer aliquet leo eu nisi elementum tincidunt. Nulla
                bibendum ante luctus nisi condimentum eleifend. Donec tortor
                magna, sagittis ac felis aliquam, pulvinar tincidunt ex. <br />
                <br />
                <br />
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam
                pulvinar enim vel tincidunt condimentum. Donec maximus arcu
                vitae lorem dignissim, et sollicitudin eros pharetra.
                Pellentesque congue venenatis turpis. Maecenas lorem massa,
                ornare in tortor at, condimentum ullamcorper metus. Duis
                ullamcorper id nulla ac congue. Cras ac sapien eu arcu bibendum
                cursus. Donec tempor placerat orci, et posuere turpis pharetra
                et. Integer aliquet leo eu nisi elementum tincidunt. Nulla
                bibendum ante luctus nisi condimentum eleifend. Donec tortor
                magna, sagittis ac felis aliquam, pulvinar tincidunt ex. <br />
                <br />
                <br />
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam
                pulvinar enim vel tincidunt condimentum. Donec maximus arcu
                vitae lorem dignissim, et sollicitudin eros pharetra.
                Pellentesque congue venenatis turpis. Maecenas lorem massa,
                ornare in tortor at, condimentum ullamcorper metus. Duis
                ullamcorper id nulla ac congue. Cras ac sapien eu arcu bibendum
                cursus. Donec tempor placerat orci, et posuere turpis pharetra
                et. Integer aliquet leo eu nisi elementum tincidunt. Nulla
                bibendum ante luctus nisi condimentum eleifend. Donec tortor
                magna, sagittis ac felis aliquam, pulvinar tincidunt ex. <br />
                <div className="justify-center text-center items-center flex">
                  <div className="relative flex overflow-hidden items-center text-center justify-center w-2/4">
                    <Image
                      src={twelve}
                      alt="An image showing the first page of a musical score to the composition FOREWORD, focusing on the usage of the first twelve tone row to construct a harmonic vamp consisting of F#mi(ma7)/A and Abma7(#5)."
                      quality="100"
                      placeholder="blur"
                    />
                  </div>
                </div>
                <p className="italic text-center">
                  Twelve Tone Matrix - Prime Row - May 12th, 2023
                  <br />G Bb D Eb Gb C B Ab A F E Db
                </p>
                <br />
                <br />
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam
                pulvinar enim vel tincidunt condimentum. Donec maximus arcu
                vitae lorem dignissim, et sollicitudin eros pharetra.
                Pellentesque congue venenatis turpis. Maecenas lorem massa,
                ornare in tortor at, condimentum ullamcorper metus. Duis
                ullamcorper id nulla ac congue. Cras ac sapien eu arcu bibendum
                cursus. Donec tempor placerat orci, et posuere turpis pharetra
                et. Integer aliquet leo eu nisi elementum tincidunt. Nulla
                bibendum ante luctus nisi condimentum eleifend. Donec tortor
                magna, sagittis ac felis aliquam, pulvinar tincidunt ex.
                <br />
                <br />
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam
                pulvinar enim vel tincidunt condimentum. Donec maximus arcu
                vitae lorem dignissim, et sollicitudin eros pharetra.
                Pellentesque congue venenatis turpis. Maecenas lorem massa,
                ornare in tortor at, condimentum ullamcorper metus. Duis
                ullamcorper id nulla ac congue. Cras ac sapien eu arcu bibendum
                cursus. Donec tempor placerat orci, et posuere turpis pharetra
                et. Integer aliquet leo eu nisi elementum tincidunt. Nulla
                bibendum ante luctus nisi condimentum eleifend. Donec tortor
                magna, sagittis ac felis aliquam, pulvinar tincidunt ex.
              </div>
            </div>
          </div>
        </div>
      </FadeInOnScroll>
    </div>
  );
}
