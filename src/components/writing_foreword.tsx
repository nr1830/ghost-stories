import Image from "next/image";

import FadeInOnScroll from "../components/fadeInOnScroll";
import foreword_01 from "../../public/foreword_001_june09_2023.png";

export default function WritingForeword() {
  return (
    <div className="w-screen bg-ghost-blue">
      <FadeInOnScroll>
        <div className="flex flex-col w-screen items-center justify-center text-center font-libre">
          <h1 id="journal01" className="xl:text-4xl lg:text-2xl py-12">
            JOURNAL 01: WRITING FOREWORD
          </h1>
          <div className="flex w-3/4 m-auto flex-wrap flex-row break-words justify-center align-middle text-center whitespace-normal">
            <p className="text-justify italic pb-5 xl:pb-10">
              A memory, lost to the soul which kept it ...
            </p>
            <div className="flex flex-wrap lg:columns-1 columns-2 text-justify">
              <div className="columns-2 gap-12">
                The rhythm section and soloist-doppleganger perform a piece
                representing a part of the soloist that has long been lost.
                <br />
                <br />
                Taking melodic inspiration from a Soviet Union-era television
                theme, the piece sets out to try to create a vignette of
                something that has long passed, while blending in elements of
                the 12-tone row which will run through the rest of the suite.
                <br />
                <br />
                <h2 className="xl:text-2xl lg:text-xl pb-5">
                  Context and Intention
                </h2>
                From a narrative perspective, I wanted this piece to be a gentle
                mood-setting introduction to the suite which serves to show
                something (perhaps a memory) that the soloist character has lost
                and will be spending the suite trying to get back.
                <br />
                <br />
                Something on my mind over the past few years has been the
                growing realization that I am losing bits of what I considered
                my identity - my first language feels like it is slowly slipping
                away, my connection to my cultural heritage, etc. I wanted to
                use this as inspiration for this suite, with the point of view
                character chasing to get a piece of those things back. This is
                why I picked the melodic shape of an old television theme I used
                to watch growing up.
                <br />
                <br />
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam
                pulvinar enim vel tincidunt condimentum. Donec maximus arcu
                vitae lorem dignissim, et sollicitudin eros pharetra.
                Pellentesque congue venenatis turpis. Maecenas lorem massa,
                ornare in tortor at, condimentum ullamcorper metus. Duis
                ullamcorper id nulla ac congue. Cras ac sapien eu arcu bibendum
                cursus. Donec tempor placerat orci, et posuere turpis pharetra
                et. Integer aliquet leo eu nisi elementum tincidunt. Nulla
                bibendum ante luctus nisi condimentum eleifend. Donec tortor
                magna, sagittis ac felis aliquam, pulvinar tincidunt ex.
                <br />
                <br />
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam
                pulvinar enim vel tincidunt condimentum. Donec maximus arcu
                vitae lorem dignissim, et sollicitudin eros pharetra.
                Pellentesque congue venenatis turpis. Maecenas lorem massa,
                ornare in tortor at, condimentum ullamcorper metus. Duis
                ullamcorper id nulla ac congue. Cras ac sapien eu arcu bibendum
                cursus. Donec tempor placerat orci, et posuere turpis pharetra
                et. Integer aliquet leo eu nisi elementum tincidunt. Nulla
                bibendum ante luctus nisi condimentum eleifend. Donec tortor
                magna, sagittis ac felis aliquam, pulvinar tincidunt ex. <br />
                <br />
                <br />
                <h2 className="xl:text-2xl lg:text-xl pb-5">
                  12-Tone Harmonic Inspiration
                </h2>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam
                pulvinar enim vel tincidunt condimentum. Donec maximus arcu
                vitae lorem dignissim, et sollicitudin eros pharetra.
                Pellentesque congue venenatis turpis. Maecenas lorem massa,
                ornare in tortor at, condimentum ullamcorper metus. Duis
                ullamcorper id nulla ac congue. Cras ac sapien eu arcu bibendum
                cursus. Donec tempor placerat orci, et posuere turpis pharetra
                et. Integer aliquet leo eu nisi elementum tincidunt. Nulla
                bibendum ante luctus nisi condimentum eleifend. Donec tortor
                magna, sagittis ac felis aliquam, pulvinar tincidunt ex.
                <br />
                <br />
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam
                pulvinar enim vel tincidunt condimentum. Donec maximus arcu
                vitae lorem dignissim, et sollicitudin eros pharetra.
                Pellentesque congue venenatis turpis. Maecenas lorem massa,
                ornare in tortor at, condimentum ullamcorper metus. Duis
                ullamcorper id nulla ac congue. Cras ac sapien eu arcu bibendum
                cursus. Donec tempor placerat orci, et posuere turpis pharetra
                et. Integer aliquet leo eu nisi elementum tincidunt. Nulla
                bibendum ante luctus nisi condimentum eleifend. Donec tortor
                magna, sagittis ac felis aliquam, pulvinar tincidunt ex.
                <br />
                <br />
                The first iteration of this piece did not contain any elements
                of the 12 tone row I had created for this suite. In the second
                week of working on the tune, I started to refactor the writing
                to try to aim for a more unsettling feeling and to include some
                elements of the row creeping in through the piece. This was
                intended to help better set the tone for the suite and make the
                jump into the less tonal writing a little less jarring.
                <br />
                <br />
                <div className="relative flex overflow-hidden items-center text-center justify-center">
                  <Image
                    src={foreword_01}
                    alt="An image showing the first page of a musical score to the composition FOREWORD, focusing on the usage of the first twelve tone row to construct a harmonic vamp consisting of F#mi(ma7)/A and Abma7(#5)."
                    quality="100"
                    placeholder="blur"
                  />
                </div>
                <p className="italic text-center">
                  FOREWORD - Page 1 - Version 1.2 - June 9th, 2023
                </p>
                <br />
                <br />
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam
                pulvinar enim vel tincidunt condimentum. Donec maximus arcu
                vitae lorem dignissim, et sollicitudin eros pharetra.
                Pellentesque congue venenatis turpis. Maecenas lorem massa,
                ornare in tortor at, condimentum ullamcorper metus. Duis
                ullamcorper id nulla ac congue. Cras ac sapien eu arcu bibendum
                cursus. Donec tempor placerat orci, et posuere turpis pharetra
                et. Integer aliquet leo eu nisi elementum tincidunt. Nulla
                bibendum ante luctus nisi condimentum eleifend. Donec tortor
                magna, sagittis ac felis aliquam, pulvinar tincidunt ex.
              </div>
            </div>
          </div>
        </div>
      </FadeInOnScroll>
    </div>
  );
}
