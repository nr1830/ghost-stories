import FadeInOnScroll from "../components/fadeInOnScroll";

export default function WritingRitual() {
  return (
    <div className="h-screen w-screen bg-ghost-blue">
      <FadeInOnScroll>
        <div className="flex flex-col h-screen w-screen items-center justify-center text-center font-libre pb-12">
          <h1 id="journal02" className="xl:text-4xl lg:text-2xl py-12">
            JOURNAL 02: WRITING RITUAL
          </h1>
          <div className="flex w-3/4 m-auto flex-wrap flex-row break-words justify-center align-middle text-center whitespace-normal">
            <p className="text-justify italic pb-5 xl:pb-10">
              Somewhere, in a darkened room, candles are lit and a lone voice
              calls out, only to hear their own echo answer them ...
            </p>
            <div className="flex flex-wrap lg:columns-1 columns-2 text-justify">
              <div className="columns-2 gap-12">
                The soloist performs an incantation to communicate with a
                many-faced ghost (the ensemble). As the two interact, each
                begins to take on an aspect of the other, communicated through
                the shifting and manipulation of the 12-tone row used as the
                foundation of the suite.
                <br />
                <br />
                <h2 className="xl:text-2xl lg:text-xl pb-5">
                  Problem Statements and Research
                </h2>
                In the initial planning phases of this suite, I tried to
                consider a number of &quot;problem statements&quot; into which I
                can break the beginnings of this project down. They fall into
                two categories: <b>(1) research</b> and{" "}
                <b>(2) pre-compositional & narrative planning</b>.
                <br />
                <br />
                In the realm of research I have two main goals; I want to focus
                on consuming ghost stories and writings about ghost stories, and
                I want to consume information about serialism and 12-tone
                composition.
                <br />
                <br />
                TODO: Please insert a list of the ghost stories you have read or
                plan to read.
                <br />
                <br />
                The following are a collection of resources I am going through
                while collecting information about 12-tone music and serialism:
                <br />
                <br />
                <li>
                  <a href="https://www.youtube.com/watch?v=18g5FCq0t0o">
                    Darcy James Argue on Visualizing Twelve Tone Rows (Jazz
                    Composers Presents Mini Lesson)
                  </a>
                </li>
                <li>
                  &quot;Twelve-Tone Music in America&quot; by Joseph N. Straus
                </li>
                <br />
                <br />
                <h2 className="xl:text-2xl lg:text-xl pb-5">World Building</h2>
                <i>
                  How does <b>the ritual</b> work?
                </i>
                <br />
                <br />
                I imagine this piece starting in a darkened room, with our
                soloist calling out into the void, hoping for an answer.
                <br />
                <br />
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam
                pulvinar enim vel tincidunt condimentum. Donec maximus arcu
                vitae lorem dignissim, et sollicitudin eros pharetra.
                Pellentesque congue venenatis turpis. Maecenas lorem massa,
                ornare in tortor at, condimentum ullamcorper metus. Duis
                ullamcorper id nulla ac congue. Cras ac sapien eu arcu bibendum
                cursus. Donec tempor placerat orci, et posuere turpis pharetra
                et. Integer aliquet leo eu nisi elementum tincidunt. Nulla
                bibendum ante luctus nisi condimentum eleifend. Donec tortor
                magna, sagittis ac felis aliquam, pulvinar tincidunt ex.
                <br />
                <br />
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam
                pulvinar enim vel tincidunt condimentum. Donec maximus arcu
                vitae lorem dignissim, et sollicitudin eros pharetra.
                Pellentesque congue venenatis turpis. Maecenas lorem massa,
                ornare in tortor at, condimentum ullamcorper metus. Duis
                ullamcorper id nulla ac congue. Cras ac sapien eu arcu bibendum
                cursus. Donec tempor placerat orci, et posuere turpis pharetra
                et. Integer aliquet leo eu nisi elementum tincidunt. Nulla
                bibendum ante luctus nisi condimentum eleifend. Donec tortor
                magna, sagittis ac felis aliquam, pulvinar tincidunt ex.
                <br />
                <br />
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam
                pulvinar enim vel tincidunt condimentum. Donec maximus arcu
                vitae lorem dignissim, et sollicitudin eros pharetra.
                Pellentesque congue venenatis turpis. Maecenas lorem massa,
                ornare in tortor at, condimentum ullamcorper metus. Duis
                ullamcorper id nulla ac congue. Cras ac sapien eu arcu bibendum
                cursus. Donec tempor placerat orci, et posuere turpis pharetra
                et. Integer aliquet leo eu nisi elementum tincidunt. Nulla
                bibendum ante luctus nisi condimentum eleifend. Donec tortor
                magna, sagittis ac felis aliquam, pulvinar tincidunt ex.
                <br />
                <br />
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam
                pulvinar enim vel tincidunt condimentum. Donec maximus arcu
                vitae lorem dignissim, et sollicitudin eros pharetra.
                Pellentesque congue venenatis turpis. Maecenas lorem massa,
                ornare in tortor at, condimentum ullamcorper metus. Duis
                ullamcorper id nulla ac congue. Cras ac sapien eu arcu bibendum
                cursus. Donec tempor placerat orci, et posuere turpis pharetra
                et. Integer aliquet leo eu nisi elementum tincidunt. Nulla
                bibendum ante luctus nisi condimentum eleifend. Donec tortor
                magna, sagittis ac felis aliquam, pulvinar tincidunt ex.
                <br />
                <br />
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam
                pulvinar enim vel tincidunt condimentum. Donec maximus arcu
                vitae lorem dignissim, et sollicitudin eros pharetra.
                Pellentesque congue venenatis turpis. Maecenas lorem massa,
                ornare in tortor at, condimentum ullamcorper metus. Duis
                ullamcorper id nulla ac congue. Cras ac sapien eu arcu bibendum
                cursus. Donec tempor placerat orci, et posuere turpis pharetra
                et. Integer aliquet leo eu nisi elementum tincidunt. Nulla
                bibendum ante luctus nisi condimentum eleifend. Donec tortor
                magna, sagittis ac felis aliquam, pulvinar tincidunt ex.
                <br />
                <br />
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam
                pulvinar enim vel tincidunt condimentum. Donec maximus arcu
                vitae lorem dignissim, et sollicitudin eros pharetra.
                Pellentesque congue venenatis turpis. Maecenas lorem massa,
                ornare in tortor at, condimentum ullamcorper metus. Duis
                ullamcorper id nulla ac congue. Cras ac sapien eu arcu bibendum
                cursus. Donec tempor placerat orci, et posuere turpis pharetra
                et. Integer aliquet leo eu nisi elementum tincidunt. Nulla
                bibendum ante luctus nisi condimentum eleifend. Donec tortor
                magna, sagittis ac felis aliquam, pulvinar tincidunt ex.
                <br />
                <br />
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam
                pulvinar enim vel tincidunt condimentum. Donec maximus arcu
                vitae lorem dignissim, et sollicitudin eros pharetra.
                Pellentesque congue venenatis turpis. Maecenas lorem massa,
                ornare in tortor at, condimentum ullamcorper metus. Duis
                ullamcorper id nulla ac congue. Cras ac sapien eu arcu bibendum
                cursus. Donec tempor placerat orci, et posuere turpis pharetra
                et. Integer aliquet leo eu nisi elementum tincidunt. Nulla
                bibendum ante luctus nisi condimentum eleifend. Donec tortor
                magna, sagittis ac felis aliquam, pulvinar tincidunt ex.
                <br />
                <br />
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam
                pulvinar enim vel tincidunt condimentum. Donec maximus arcu
                vitae lorem dignissim, et sollicitudin eros pharetra.
                Pellentesque congue venenatis turpis. Maecenas lorem massa,
                ornare in tortor at, condimentum ullamcorper metus. Duis
                ullamcorper id nulla ac congue. Cras ac sapien eu arcu bibendum
                cursus. Donec tempor placerat orci, et posuere turpis pharetra
                et. Integer aliquet leo eu nisi elementum tincidunt. Nulla
                bibendum ante luctus nisi condimentum eleifend. Donec tortor
                magna, sagittis ac felis aliquam, pulvinar tincidunt ex.
                <br />
                <br />
                <br />
                <br />
              </div>
            </div>
          </div>
        </div>
      </FadeInOnScroll>
    </div>
  );
}
