import FadeInOnScroll from "../components/fadeInOnScroll";
import Link from "next/link";

export default function TOC() {
  return (
    <div className="h-screen w-screen bg-ghost-blue">
      <FadeInOnScroll>
        <div className="flex h-screen w-screen font-libre">
          <div className="w-1/2 items-center justify-center align-middle text-center m-auto">
            <h1 className="text-4xl p-20">TABLE OF CONTENTS</h1>

            <Link href="#introduction">
              <div className="flex flex-row items-center text-center justify-between mx-22 p-6">
                <h2 className="lg:text-1xl xl:text-2xl">00</h2>
                <h2 className="lg:text-1xl xl:text-2xl">Introduction</h2>
              </div>
            </Link>

            <Link href="#journal00">
              <div className="flex flex-row items-center text-center justify-between mx-22 p-6">
                <h2 className="lg:text-1xl xl:text-2xl">01</h2>
                <h2 className="lg:text-1xl xl:text-2xl">
                  Journal 00: Project Concept
                </h2>
              </div>
            </Link>

            <Link href="#journal01">
              <div className="flex flex-row items-center text-center justify-between mx-22 p-6">
                <h2 className="lg:text-1xl xl:text-2xl">02</h2>
                <h2 className="lg:text-1xl xl:text-2xl">
                  Journal 01: Writing Foreword
                </h2>
              </div>
            </Link>

            <Link href="#journal02">
              <div className="flex flex-row items-center text-center justify-between mx-22 p-6">
                <h2 className="lg:text-1xl xl:text-2xl">03</h2>
                <h2 className="lg:text-1xl xl:text-2xl">
                  Journal 02: Writing Ritual
                </h2>
              </div>
            </Link>

            <Link href="#journal03">
              <div className="flex flex-row items-center text-center justify-between mx-22 p-6">
                <h2 className="lg:text-1xl xl:text-2xl">03</h2>
                <h2 className="lg:text-1xl xl:text-2xl">
                  Journal 03: Writing Dreams
                </h2>
              </div>
            </Link>
          </div>
        </div>
      </FadeInOnScroll>
    </div>
  );
}
