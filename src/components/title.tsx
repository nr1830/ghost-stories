import Image from "next/image";
import title from "../../public/ghost_stories_title_01.png";
import FadeInOnScroll from "../components/fadeInOnScroll";

export default function Title() {
  return (
    <div className="h-screen w-screen bg-ghost-blue">
      <FadeInOnScroll>
        <div className="flex h-screen w-screen">
          <div className="items-center justify-center align-middle text-center m-auto">
            <Image
              src={title}
              alt="GHOST STORIES FOR LARGE ENSEMBLE Logo"
              width="750"
              height="375"
              quality="100"
              placeholder="blur"
            />
          </div>
        </div>
      </FadeInOnScroll>
    </div>
  );
}
