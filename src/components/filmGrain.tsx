import { useEffect, useRef } from "react";

interface GrainOverlayProps {
  opacity?: number;
}

const GrainOverlay: React.FC<GrainOverlayProps> = ({ opacity = 0.1 }) => {
  const canvasRef = useRef<HTMLCanvasElement>(null);

  useEffect(() => {
    const canvas = canvasRef.current;
    if (!canvas) return;

    const context = canvas.getContext("2d");
    if (!context) return;

    const sketch = () => {
      const { width, height } = canvas;

      context.clearRect(0, 0, width, height);

      const grainSize = 0.5;
      const grainScale = 0.025;
      for (let x = 0; x < width; x += grainSize) {
        for (let y = 0; y < height; y += grainSize) {
          const grain = Math.random() * grainScale;
          const color = Math.floor(255 - grain * 255);
          context.fillStyle = `rgb(${color},${color},${color})`;
          context.fillRect(x, y, grainSize, grainSize);
        }
      }

      requestAnimationFrame(sketch);
    };

    sketch();
  }, []);

  return (
    <canvas
      ref={canvasRef}
      className="fixed inset-0 w-full h-full z-50"
      style={{
        zIndex: 50,
        opacity,
      }}
    />
  );
};

export default GrainOverlay;
