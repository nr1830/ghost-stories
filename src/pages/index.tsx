import Title from "../components/title";
import TOC from "../components/toc";
import Introduction from "../components/introduction";
import WritingRitual from "../components/writing_ritual";
import WritingForeword from "../components/writing_foreword";
import WritingDreams from "../components/writing_dreams";
import Concept from "../components/project_concept";

// TODO: Revise this overlay component ... it doesn't look great.
import GrainOverlay from "../components/filmGrain";

export default function Home() {
  return (
    <main className="scrollbar-hide overflow-auto whitespace-nowrap bg-ghost-blue">
      <Title />
      <TOC />
      <Introduction />
      <div className="flex-col lg:space-y-40">
        <Concept />
        <WritingForeword />
        <WritingRitual />
        <WritingDreams />
      </div>
    </main>
  );
}
